import { Module } from '@nestjs/common'
import { mongodbConnectionProvider } from '../provider/mongodb-connection.provider'
import { ConfigModule } from './config.module'

@Module({
    imports: [
      ConfigModule
    ],
    providers: [
        mongodbConnectionProvider
    ],
    exports: [
        mongodbConnectionProvider
    ]
})
export class MongodbConnectionModule {

}
