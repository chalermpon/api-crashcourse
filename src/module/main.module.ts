import { Module } from '@nestjs/common'
import { MongodbConnectionModule } from './mongodb-connection.module'

@Module({
    imports: [
        MongodbConnectionModule
    ],
    providers: [],
    controllers: [],
    exports: []

})
export class MainModule {}
