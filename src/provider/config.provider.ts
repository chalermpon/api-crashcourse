import { ValueProvider } from '@nestjs/common/interfaces'
import { ProviderName } from './index'
import * as Config from 'config'

export const configProvider: ValueProvider = {
    provide: ProviderName.CONFIG_PROVIDER,
    useValue: Config.util.toObject()
}
