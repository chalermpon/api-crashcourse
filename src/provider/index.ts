export enum ProviderName {
    CONFIG_PROVIDER = 'config-provider',
    MONGODB_CONNECTION_PROVIDER = 'mongodb-connection-provider',
}
